//Axel Brochu 2233104
package rpsgame;

import java.util.InputMismatchException;
import java.util.Scanner;

import backend.RpsGame;

/**
 * Console application for rock paper scissors game
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        final String[] possiblePicks = {"rock", "paper", "scissors"};
        boolean exit = false;
        RpsGame game = new RpsGame();
        Scanner kb = new Scanner(System.in);
        while(!exit){
            boolean valid = false;
            int move = 0;
            while(!valid){
                System.out.println("Pick a move! (0: rock, 1: paper, 2: scissors)");
                try{
                   move = kb.nextInt();
                   kb.nextLine();
                   valid = true;
                }catch(InputMismatchException e){
                    kb.nextLine();
                    System.out.println("Please enter a number");
                }
            }

            System.out.println(game.playRound(possiblePicks[move]));

            System.out.println("Wins " + game.getWins() + " Losses " + game.getLosses() + " Ties " + game.getTies());

            char option = ' ';
            valid = false;
            while(!valid){
                try {
                    System.out.println("Would you like to exit? (y/n)");
                    option = kb.nextLine().toLowerCase().charAt(0);
                    valid = true;
                } catch (StringIndexOutOfBoundsException e) {
                    System.out.println("Enter something please!");
                }
            }
            if(option == 'y')
                exit = true;
        }
    }
}
