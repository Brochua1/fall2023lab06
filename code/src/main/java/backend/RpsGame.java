//Axel Brochu 2233104
package backend;

import java.util.Random;

public class RpsGame {
    private int ties;
    private int wins;
    private int losses;
    private Random rng;

    public RpsGame(){
        ties = 0;
        wins = 0;
        losses = 0;
        rng = new Random();
    }

    public int getTies() {
        return ties;
    }

    public int getWins() {
        return wins;
    }

    public int getLosses() {
        return losses;
    }

    public String playRound(String playerPick){
        String[] possiblePicks = {"rock", "paper", "scissors"};
        playerPick = playerPick.toLowerCase();
        
        int computerNumber = rng.nextInt(3);
        String computerPick = possiblePicks[computerNumber];

        String picksString = "You picked " + playerPick + ", Computer picked " + computerPick;

        if(playerPick.equals(computerPick)){
            ties++;
            return picksString + ", its a tie!";
        }

        if(playerPick.equals("paper") && computerPick.equals("rock")){
            wins++;
            return picksString + ", you won";
        }

        if(playerPick.equals("scissors") && computerPick.equals("paper")){
            wins++;
            return picksString + ", you won";
        }

        if(playerPick.equals("rock") && computerPick.equals("scissors")){
            wins++;
            return picksString + ", you won";
        }

        losses++;
        return picksString + ", you lost";
    }
    
}
